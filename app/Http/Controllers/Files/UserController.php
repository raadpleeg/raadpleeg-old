<?php namespace App\Http\Controllers\Files;

use Illuminate\Routing\Controller;

class UserController extends Controller
{
    private $targetPath = "uploads/";


    /**
     * @return \Illuminate\Http\JsonResponse of all the files in the uploads map.
     */
    public function index()
    {
        return response()->json($this->getGlob());
    }

    /** Creates a .txt file in the uploads map.
     * @return \Illuminate\Http\JsonResponse of the filename generated
     */
    public function create()
    {
        do {
            $filename = $this->createFileName();
        } while (file_exists($this->targetPath . $filename . ".txt"));

        fopen($this->targetPath . $filename . ".txt", "w");

        return response()->json(["Filename" => $filename]);
    }

    /**
     * @return array of the files on the server
     */
    private function getGlob()
    {
        return glob("uploads/*");

    }

    /** Generates a random filename to use.
     * @return string randomly generated filename
     */
    private function createFileName()
    {
        $startString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $name = str_split($startString);
        $nameToSet = "";
        for ($i = 0; $i <= 9; $i++) {
            $nameToSet = $nameToSet . $name[random_int(0, 61)];
        }
        return $nameToSet;
    }

}